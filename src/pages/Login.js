import { Form, Button, Row, Col, Nav } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

import { useNavigate, Navigate, Link, NavLink } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login(){

	// Allows us to consume the User Context object and it's properties to use for user validation
	const { user, setUser } = useContext(UserContext);

	// hook return a function that lets you navigate to components.
	//const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState("true")

	function authenticate(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
			} else {
				Swal.fire({
					title: "Authentication Failed!",
					icon: 'error',
					text: 'Please, check you login details and try again!'
				})
			}
		});

		

		setEmail("");
		setPassword("");
		
	};

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin,
				cart: data.cart
			});

			console.log("")
		});
	};



	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [email, password]);

	
	return (

		
		(user.id !== null) ? 
			<Navigate to="/products" />
		:


		
		<Form id="Login" onSubmit={(e) => authenticate(e)}>
			<div className="mb-4 text-center">
			<Form.Group controlId="userEmail">
				{/*<Form.Label>Email Address</Form.Label>*/}
				<input
					type="email"
					placeholder="Email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
			</Form.Group>
			</div>
			<div className="mb-4 text-center">
			<Form.Group controlId="Password">
				{/*<Form.Label>Password</Form.Label>*/}
				<input
					type="password"
					placeholder="password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
			</Form.Group>
			</div>
			<div className="text-center">
			{ isActive ? 

				<Button variant="dark my-3" type="submit" id="submitBtn" width="100">Log in</Button>

				: 

				<Button variant="outline-dark my-3" type="submit" id="submitBtn" width="100" disabled>Log in</Button>

			}

			 <h5 className ="text-center text-divider"></h5>
			<p>Don't have an account yet? <Link to="/register">Register Here</Link></p>
			</div>
		</Form>
		
		
	)

}

