import {Button, Row, Col, Nav} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';

export default function errorPage() {
	return(
			<Row>
				<Col>
					<h1>Oppss!</h1>
					<h1>This page don't exist</h1>
					<p>Go back to the <Link to="/">homepage</Link></p>
				</Col>
			</Row>
					
		)
}

