import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Carousel from 'react-bootstrap/Carousel';

export default function Home() {

	const data = {
		title: "Welcome to pinkMango Co.",
		content: "Hi I'm Michael Nino Ballesca a web developer based in the Philippines. Welcome to pinkMango Co. a eCommerce Full-Stack  website I built and developed using the MERN Stack infused with working E-Commerce API using Node and Express.js. Enjoy shopping!",
		destination: "/products",
		label: "Shop Now!"
	}
	return(
		<>
		<Banner data={data} />
		<br/>
		<Carousel variant="dark" >
		      <Carousel.Item interval={1000}>
		        <img
		          className="d-block w-100"
		          src="https://iili.io/HvY1ifR.png"
		          alt="First slide"
		        />
		        {/*<Carousel.Caption>
		          {/*<h5>First slide label</h5>
		          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>}
		  			 </Carousel.Caption>*/}
		      </Carousel.Item>
		      <Carousel.Item>
		        <img
		          className="d-block w-100"
		          src="https://iili.io/HvYEosj.png"
		          alt="Second slide"
		        />
		        {/*<Carousel.Caption>
		          <h5>Second slide label</h5>
		          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		        </Carousel.Caption>*/}
		      </Carousel.Item>
		      <Carousel.Item interval={1000}>
		        <img
		          className="d-block w-100"
		          src="https://iili.io/HvYEI0Q.png5"
		          alt="Third slide"
		        />
		        <Carousel.Caption interval={1000}>
		          {/*<h5>Third slide label</h5>
		          <p>
		            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
		          </p>*/}
		        </Carousel.Caption>
		      </Carousel.Item>
		    </Carousel>

			
			<Highlights />
		</>
	)
}
