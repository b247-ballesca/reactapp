import { Form, Button, Card } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'
import InputGroup from 'react-bootstrap/InputGroup';

import {useParams, useNavigate, navigat, link} from 'react-router-dom' 

import Swal from 'sweetalert2';



export default function ViewProduct() {



	const { user } = useContext(UserContext);
	const navigate = useNavigate("");

	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [salePrice, setSalePrice] = useState("");
	const [itemGroupId, setItemGroupId] = useState("");
	const [link, setLink] = useState("");
	const [color, setColor] = useState("");
	const [size, setSize] = useState("");
	const [category, setCategory] = useState("");
	const [quantity, setQuantity] = useState("");

	const [isActive, setIsActive] = useState(false);

	const {productId} = useParams();

	function EditProduct(e){

		
		// fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
		// 	method: "PUT",
		// 	headers: {
		// 		'Content-Type': 'application/json',
		// 		Authorization: `Bearer ${localStorage.getItem('token')}`
		// 	},
		// 	body: JSON.stringify({
		// 		productName: productName,
		// 		description: description,
		// 		price: price,
		// 		salePrice: salePrice,
		// 		itemGroupId: itemGroupId,
		// 		link: link,
		// 		color: color,
		// 		size: size,
		// 		category: category,
		// 		isActive: isActive,
		// 		quantity: quantity
		// 	})
		// })
		// .then(res => res.json())
	 	// .then(data => {
	 	// 	if (data) {

	 	// 		Swal.fire({
	 	// 			text: `Product ${productName} succesfully updated!`,
	 	// 			icon: "success",
	 	// 			title: "Great Job!"

	 	// 		})

	 	// 		setProductName("");
		// 		setDescription("");
		// 		setPrice("");
		// 		setSalePrice("");
		// 		setItemGroupId("");
		// 		setLink("");
		// 		setColor("");
		// 		setSize("");
		// 		setCategory("");
		// 		setIsActive(false);
		// 		setQuantity("");
		// 		navigate("/products");

	 	// 	}else{
	 	// 		Swal.fire({
	 	// 			text: "Opps Something went wrong",
	 	// 			icon: "error",
	 	// 			title: "Please try again!"
	 	// 		})
	 	// 	}
	 	// })

	}

	 const pushProduct = (e) => {
	 	e.preventDefault()

	 	
	 		
	 		 EditProduct();
	 		
	 	
	 }

	 
let sum

if ( user.cart.length != null && user.cart.length != undefined && user.cart.length != 0) {
  let priceArray = user.cart.map((product) => product.price)
  sum = priceArray.reduce((accumulator, currentValue) => {return accumulator + currentValue}, 0)
} else {
  sum = 0
}


return (
     
	<div className = "container-fluid">
		<div className="text-left row">

			<div className = "col-8" >
				<h3 className = "pt-3">Customer Info</h3>
					<div className = "row">	
						<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>First Name</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="Juan"
							        value = ""
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
						</Form>	
						</div>
						<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Last Name</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="Dela Cruz"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
				    	<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Email</Form.Label>
						        <Form.Control 
							        type="email" 
							        placeholder="xyz@example.com"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
				    	<div className="col-8">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Address</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="122 Example Street"
							        value = ""
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
						</Form>	
						</div>
				    	<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Country</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="Philippines"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
				    	<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Town/City</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="Quezon City"
							        value = ""
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
						</Form>	
						</div>
						<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>State/Province</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="NCR"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
				    	<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Postal Code</Form.Label>
						        <Form.Control 
							        type="number" 
							        placeholder="0000"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
			    	</div>
			    <h3 className = "pt-3">Payment Info</h3>
			    	<div className = "row">	
			    		<div className = "col-12 row">
							<div className="col-3">
							<Form id="EditProduct" onSubmit={pushProduct}>
					        	<Form.Group controlId="productName" className="my-2">
						        <Form.Label className= "p-2" >Debit Card</Form.Label>
							    <input name="gateway" type="radio" value="test_gateway">
							        	
							    </input>
					    		</Form.Group>
							</Form>	
							</div>
							<div className="col-3">
							 <Form id="EditProduct" onSubmit={pushProduct}>
					        	<Form.Group controlId="productName" className="my-2">
						        <Form.Label className= "p-2" >Credit Card</Form.Label>
							    <input name="gateway" type="radio" value="test_gateway">
							        	
							    </input>
					    		</Form.Group>
							</Form>	
							</div>
						</div>
						
						<div className = "col-12 row">
				    		<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Card Number</Form.Label>
						        <Form.Control 
							        type="number" 
							        placeholder="0001100011002200"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    		</Form>	
					    	</div>
					    	<div className="col-4">
							 <Form id="EditProduct" onSubmit={pushProduct}>
					        	<Form.Group controlId="productName" className="my-2">
						        <Form.Label>Billing Zip</Form.Label>
							        <Form.Control 
								        type="number" 
								        placeholder="0000"
								        value = ""
								        onChange = {e => setProductName(e.target.value)} 
								        required
							        /> 
					    		</Form.Group>
							</Form>	
							</div>
							</div>
				    	
				    	<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Month/Year</Form.Label>
						        <Form.Control 
							        type="month" 
							        placeholder="00"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
						<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>CVC</Form.Label>
						        <Form.Control 
							        type="number" 
							        placeholder="000"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
			    	</div>	
			    <h3 className = "pt-3">Billing Info</h3>
					<div className = "row">	
						
				    	<div className="col-8">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Billing Name</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="Juan Dela Cruz"
							        value = ""
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
						</Form>	
						</div>
				    	<div className="col-4">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Country</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="Philippines"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
				    	<div className="col-3">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Address</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="Quezon City"
							        value = ""
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
						</Form>	
						</div>
						<div className="col-3">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>City</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="Quezon City"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
				    	<div className="col-3">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>State/Province</Form.Label>
						        <Form.Control 
							        type="text" 
							        placeholder="NCR"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
				    	<div className="col-3">
						 <Form id="EditProduct" onSubmit={pushProduct}>
				        	<Form.Group controlId="productName" className="my-2">
					        <Form.Label>Postal Code</Form.Label>
						        <Form.Control 
							        type="number" 
							        placeholder="0000"
							        value = {productName}
							        onChange = {e => setProductName(e.target.value)} 
							        required
						        /> 
				    		</Form.Group>
				    	</Form>	
				    	</div>
			    	</div>	
			</div>

			<div className = "col-4">
			<Card>
				<Card.Title>
				<h3 className ="text-center text-divider"><span>Current Cart</span></h3>
				</Card.Title>
				<Card.Body>

				    {user.cart.map(item => (
				    <div id="cartItems" className="row" >
				            <div className="p-2 col-4 text-left text-danger" > {item.product_name} </div>
				            <div className="p-2 col-4 text-left"> x{item.quantity} </div>
				            <div className="p-2 col-4 text-center">PHP {item.price} </div>
				        
				    </div>
				   
				    ))}

				    <h5 className ="text-center text-divider" ><span>Shipping Option</span></h5> 
				            <Form.Group controlId="itemGroupId" className="my-2 text-center">
				    	        
				    		        <Form.Control 
				    			        type="text" 
				    			        placeholder="Select Shipping Method"
				    			        
				    			        onChange = {e => setItemGroupId(e.target.value)}
				    		        />
				    		        <Form.Text className="text-danger">
				    		        	Select Country for Shipping Options
				    		        </Form.Text>
				            </Form.Group>
				    <h5 className ="text-center text-divider" ><span>Discount Code</span></h5> 
				            <InputGroup className="mb-3">
				                   <Form.Control
				                     placeholder=""
				                     aria-label="Recipient's username"
				                     aria-describedby="basic-addon2"
				                   />
				                   <Button variant="dark" id="button-addon2">
				                     Apply
				                   </Button>
				            </InputGroup>
        
				    
				    <h5 className ="text-center text-divider" ><span>Cart Total</span></h5> 
				    <h3 className ="text-center" >PHP {sum}</h3>
				    
				
				</Card.Body>
			</Card>
			</div>
	    <div className ="col-8">   
	         { (user.id != null) ?
	    	 <>
	         <Button className ="m-3" variant="dark" type="submit" id="submitBtn">
	         Complete Checkout and Page</Button>
	         </>
	        :
	      	
	       	 <Button className ="m-3" variant="outline-dark" as={link} to="/login">Log in to Checkout this item
	       	 </Button>

			
			 }

			<Button className ="m-3" variant="outline-dark" type="submit" id="cancelBtn" as={link} to={`/products`}>
	        Cancel
	        </Button>
	    </div>    
	</div>
	</div>	
     )

}
