import { Navigate } from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
 
import UserContext from '../UserContext'
	

export default function Logout() {
	// localStorage.clear()// 

	const { unsetUser, setUser} = useContext(UserContext);

	unsetUser();

	useEffect(() =>{
		setUser({id: null,
                 isAdmin: null,
                 cart: 0})
	})
	

	return(
		<Navigate to="/login" />
	)
}