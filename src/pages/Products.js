//import coursesData from '../data/coursesData';
import { Card, Button, Row, Col } from 'react-bootstrap';
import CardGroup from 'react-bootstrap/CardGroup';
import ProductCard from '../components/ProductCard';
import Loading from '../components/Loading';


import { useState, useEffect, useContext, useReducer } from 'react';

import UserContext from '../UserContext';

export default function Products() {

	
	const [ products, setProduct ] = useState([]);
	const [ isLoading, setIsLoading ] = useState([]);
	const { user, setUser } = useContext(UserContext);
	let showProducts = ''

	console.log(user)

	useEffect(() => {
		// (user.isAdmin === true) ?
        //     showProducts = '/products'
        //     :
        //     <>
        //     </>
        //     showProducts = '/products/active'
    //       
	if (user.isAdmin === true)  { 
	  showProducts = '/products'
	} else {
      showProducts = '/products/active'
    }

		fetch(`${process.env.REACT_APP_API_URL}`+ showProducts)
		.then(res => res.json())
		.then(data => {
			console.log(data);

				setProduct(data.map(product => {
				return(
					
					<ProductCard key={product._id} product={product} setIsLoading ={setIsLoading} />
				)
				}))

			setIsLoading(false);
			
		})


	}, [])

	

	return(
		(isLoading)?
		<Loading />
		:
		<div className="container-fluid row" >
			<div className="col-4">

			<img src="https://iili.io/Hv59hga.gif" id="ProdutPageImage"/ >
			<img src="https://iili.io/Hv5Jf9f.png" id="ProdutPageImage1"/ >
			</div>
			<div className="col-8">
				<div className="row">
				 			{products}
			    </div>
		    </div>	  
        </div>       
	)

}

//products