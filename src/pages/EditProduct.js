import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'

import {useParams, useNavigate, navigate, link} from 'react-router-dom' 

import Swal from 'sweetalert2';



export default function ViewProduct() {



	const { user } = useContext(UserContext);
	const navigate = useNavigate("");

	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [salePrice, setSalePrice] = useState("");
	const [itemGroupId, setItemGroupId] = useState("");
	const [link, setLink] = useState("");
	const [color, setColor] = useState("");
	const [size, setSize] = useState("");
	const [category, setCategory] = useState("");
	const [quantity, setQuantity] = useState("");

	const [isActive, setIsActive] = useState(false);

	const {productId} = useParams();

	function editProduct(e){

		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				salePrice: salePrice,
				itemGroupId: itemGroupId,
				link: link,
				color: color,
				size: size,
				category: category,
				isActive: isActive,
				quantity: quantity
			})
		})
		.then(res => res.json())
	 	.then(data => {
	 		if (data) {

	 			Swal.fire({
	 				text: `Product ${productName} succesfully updated!`,
	 				icon: "success",
	 				title: "Great Job!"

	 			})

	 			setProductName("");
				setDescription("");
				setPrice("");
				setSalePrice("");
				setItemGroupId("");
				setLink("");
				setColor("");
				setSize("");
				setCategory("");
				setIsActive(false);
				setQuantity("");
				navigate("/products");

	 		}else{
	 			Swal.fire({
	 				text: "Opps Something went wrong",
	 				icon: "error",
	 				title: "Please try again!"
	 			})
	 		}
	 	})

	}

	 const pushProduct = (e) => {
	 	e.preventDefault()

	 	
	 		
	 		 editProduct();
	 		
	 	
	 }

	 useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			
			setProductName(data.product_name);
			setDescription(data.description);
			setPrice(data.price);
			setSalePrice(data.sale_price);
			setItemGroupId(data.item_group_id);
			setColor(data.color);
			setSize(data.size);
			setCategory(data.category);
			setIsActive(data.isActive);
			setQuantity(data.quantity);

		})
	}, [productId])

	useEffect(() => {
		if((productName !== "" && price !== "")){
			setIsActive(true)
		} 
	}, [productName, price]);



	return (
        //  (user.isAdmin === true) ?
        // <Navigate to ="/products"/>

        // :

        <Form id="EditProduct" onSubmit={pushProduct}>
	        <Form.Group controlId="productName" className="my-2">
		        <Form.Label>Product Name</Form.Label>
			        <Form.Control 
				        type="text" 
				        placeholder="Enter a Product Name/Title"
				        value = {productName}
				        onChange = {e => setProductName(e.target.value)} 
				        required
			        /> 
	        </Form.Group>

        <Form.Group controlId="description" className="my-2">
        	<Form.Label>Description</Form.Label>
		        <Form.Control 
			        type="text" 
			        placeholder="Enter Product Description"
			        value = {description}
			        onChange = {e => setDescription(e.target.value)} 
			    /> 
        </Form.Group>

        <Form.Group controlId="price" className="my-2">
	        <Form.Label>Price</Form.Label>
		        <Form.Control 
			        type="number" 
			        placeholder="Enter Price"
			        value = {price}
			        onChange = {e => setPrice(e.target.value)} 
			        required
			        />
        </Form.Group>

        


        <hr  style={{
            color: '#000000',
            backgroundColor: '#000000',
            height: .5,
            borderColor : '#000000',
            margin: '50px',
        }}/> 


        <Form.Group controlId="salePrice" className="my-2">
	        <Form.Label>Sale Price</Form.Label>
		        <Form.Control 
			        type="text" 
			        placeholder="Enter Sale Price"
			        value = {salePrice}
			        onChange = {e => setSalePrice(e.target.value)} 
		        /> 
        </Form.Group>

        <Form.Group controlId="itemGroupId" className="my-2">
	        <Form.Label>Item Group ID</Form.Label>
		        <Form.Control 
			        type="text" 
			        placeholder="Enter Parent Level ID"
			        value = {itemGroupId}
			        onChange = {e => setItemGroupId(e.target.value)}
		        />
		        {/*<Form.Text className="text-muted">
		        	We'll never share your email with anyone else.
		        </Form.Text>*/}
        </Form.Group>

        <Form.Group controlId="color" className="my-2">
	        <Form.Label>Color</Form.Label>
		        <Form.Control 
			        type="text" 
			        placeholder="Color" 
			        value = {color}
			        onChange = {e => setColor(e.target.value)}
		        />
		        {/*<Form.Text className="text-muted">
		        	Must be 8 characters or more!
		        </Form.Text>*/}
        </Form.Group>

        <Form.Group controlId="size" className="my-2">
	        <Form.Label>Size</Form.Label>
		        <Form.Control 
			        type="text" 
			        placeholder="Solor" 
			        value = {size}
			        onChange = {e => setSize(e.target.value)}
			        
		        />
		        {/*<Form.Text className="text-muted">
		        	Must be 8 characters or more!
		        </Form.Text>*/}
        </Form.Group>

        <Form.Group controlId="category" className="my-2">
	        <Form.Label>Category</Form.Label>
		        <Form.Control 
			        type="text" 
			        placeholder="Category" 
			        value = {category}
			        onChange = {e => setCategory(e.target.value)}
			        
		        />
		        {/*<Form.Text className="text-muted">
		        	Must be 8 characters or more!
		        </Form.Text>*/}
        </Form.Group>

        <Form.Group controlId="isActive" className="my-2">
	        <Form.Label>Active</Form.Label>
	        <br/>
		        <select
			        type="text" 
			        placeholder="false" 
			        value = {isActive}
			        onChange = {e => setIsActive(e.target.value)}
		        >
		        <option value="true">true</option>
                <option value="false">false</option>
		        </select>
		    <br/>    
		        {/*<Form.Text className="text-muted">
		        	Plea
		        </Form.Text>*/}
        </Form.Group>

        <Form.Group controlId="quantity" className="my-2">
	        <Form.Label>Quantity</Form.Label>
		        <Form.Control 
			        type="number" 
			        placeholder="0" 
			        value = {quantity}
			        onChange = {e => setQuantity(e.target.value)}
			        
		        />
		        {/*<Form.Text className="text-muted">
		        	Must be 8 characters or more!
		        </Form.Text>*/}
        </Form.Group>

        { isActive ?
        <Button variant="primary" type="submit" id="submitBtn" >
        Save Product
        </Button>
        :
        <Button variant="danger" disabled type="submit" id="submitBtn">
        Save Product
        </Button>
		}

		<Button variant="danger" type="submit" id="cancelBtn" as={link} to={`/products`}>
        Cancel
        </Button>

    </Form>
    )
}
