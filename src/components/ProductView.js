import { useParams, useNavigate, Link } from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import UserContext from '../UserContext';

import Swal from 'sweetalert2'

export default function CouseView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();

	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [salePrice, setSalePrice] = useState(0);
	const [itemGroupId, setItemGroupId] = useState("");
	const [link, setLink] = useState("");
	const [color, setColor] = useState("");
	const [size, setSize] = useState("");
	const [category, setCategory] = useState("");
	const [quantity, setQuantity] = useState("");
	const [isActive, setIsActive] = useState(false);
	const [image_link, setImageLink] = useState(false);
const [modalShow, setModalShow] = useState(true);

	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},  
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: "Successfully Enrolled",
					icon: "success",
					text: "You have successfully enrolled for this course."
				})
				navigate("/courses");
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}



	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductName(data.product_name);
			setDescription(data.description);
			setPrice(data.price);
			setSalePrice(data.sale_price);
			setItemGroupId(data.item_group_id);
			setColor(data.color);
			setSize(data.size);
			setCategory(data.category);
			setIsActive(data.isActive);
			setQuantity(data.quantity);
			setImageLink(data.image_link);
		})
	}, [productId])


function MyVerticallyCenteredModal(props) {

return (	
	<Modal
			{...props}
		  size="lg"
		  aria-labelledby="contained-modal-title-vcenter"
		  centered
		>
		<Modal.Body>
			<Container>
				<Row>
					<Col lg={/*{span: 12, offset:3}*/{span: 12}} >
						<Card id="ProductView">
						      <Card.Body className="text-left">
						      <div className="container">
						      <div className="row">
							      <div className="col-4">
							       <Card.Img src={image_link}  id="ProductImages" />
							      </div>
							      <div className="col-8">
							        <Card.Title><h1>{productName}</h1></Card.Title>
							        <Card.Subtitle><h3 className="text-danger fw-bold" >PHP {price}</h3></Card.Subtitle>
							        <Card.Subtitle><h5>{description}</h5></Card.Subtitle>
							         <h5 className ="text-center text-divider" ></h5> 
							        <Card.Subtitle>Size:</Card.Subtitle>
							        <Card.Text>{size}</Card.Text>
							        <Card.Subtitle>Color:</Card.Subtitle>
							        <Card.Text>{color}</Card.Text>

							      </div>
							  </div>
						      </div>
						      </Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
			
        
		</Modal.Body>
       
        {/*<h6 floated='right' >TOTAL AMOUNT: PHP </h6> */}
    
        
        <Modal.Footer>
        <Button onClick={props.onHide} as={Link} variant="outline-dark" to="/products">Close</Button>
	        {
			(user.id !== null) ?
				<>
		        <Button onClick={props.onHide} variant="dark">Check Out</Button>
		        </>
			:
				<Button variant="outline-dark" as={Link} to="/login">Log in to Checkout this item</Button>
			}
        </Modal.Footer>
        </Modal>
 )       
}	

	return (
               <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                />
		)
};