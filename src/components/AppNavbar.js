
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {useEffect, useState, useContext} from 'react';
import { BsFillCartCheckFill, BsFillCartPlusFill } from 'react-icons/bs'
import { AiFillPlusSquare, AiFillMinusSquare } from 'react-icons/ai'
import {BiSearchAlt} from 'react-icons/bi'
import { Card, Button, Item, Form, InputGroup } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';

import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function AppNavbar() {

  
  const { user, setUser } = useContext(UserContext); 
  const { total, setTotal } = useState(user.cart);
  const [ quantitycount, setQuantityCount ] = useState(0);
  const [ product_id, setProductId ] = useState("");
  let removeItemsCart = ""
  let cartIsEmpty = false
 
  const [modalShow, setModalShow] = useState(false);
  let updatedCart = []

  console.log(user.cart);
 



let sum

if ( user.cart.length != null && user.cart.length != undefined && user.cart.length != 0) {
  let priceArray = user.cart.map((product) => product.price)
  sum = priceArray.reduce((accumulator, currentValue) => {return accumulator + currentValue}, 0)
} else {
  sum = 0
}





 function MyVerticallyCenteredModal(props) {



  // const {product_name, product_id, price, quantity} = cartItems;

  const emptyCart = () => {
        //e.preventDefault()
        

        fetch(`${process.env.REACT_APP_API_URL}/users/emptycart`, {
                        method: "PUT",
                        headers: {
                                'Content-Type': 'application/json',
                                Authorization: `Bearer ${localStorage.getItem('token')}`
                        }
                        })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data);

                             setUser({
                                id: user.id,
                                isAdmin: user.isAdmin,
                                cart: []
                              });
                            
                             console.log(user);
                           
                        })

              props.onHide(true)


  }

  async function AddQuantityButton(e) {
   
    const updatedItemList = user.cart.map((item) => {
       if (item.product_id === e) {
          return { ...item, quantity: item.quantity + 1, price: item.price * (item.quantity + 1)};
       }
       return item;
      });
     updatedCart = updatedItemList
  }
        
  async function AddQuantity(e){  

    if (e.target.getAttribute('a-key') != null) {

      await AddQuantityButton(e.target.getAttribute('a-key'))    
      console.log(updatedCart)
      UpdateCart()

    }
  }

async function FilterCartItems(e){
  console.log(e)
    removeItemsCart = user.cart.filter(remove => remove.product_id != e)

  }

async function DecreaseQuantityButton(e){

    removeItemsCart=""
  
    const updatedItemList =  await user.cart.map((item) => {
        if (item.product_id === e) {
        
          if (item.quantity === 1 && user.cart.length > 1 ){
               FilterCartItems(e)
          } else if (item.quantity === 1 && user.cart.length === 1){ 
              cartIsEmpty = true
          } else {

             return { ...item, quantity: item.quantity - 1, price: (item.price / item.quantity ) * (item.quantity - 1)};

          }
             
    
        console.log(item)    
        
      }
       return item
      });

    console.log(updatedItemList.length )

    if (cartIsEmpty){
      updatedCart = []
      props.onHide(true)

    } else if (removeItemsCart != "") {

      updatedCart = removeItemsCart
      
    }else{
       updatedCart = updatedItemList
    }
      

      console.log(updatedItemList)
      console.log(updatedCart) 
}

 async function DecreaseQuantity(e){  


  console.log(user.cart)
  console.log(e.target.getAttribute('a-key'))
    if (e.target.getAttribute('a-key') != null || e.target.getAttribute('a-key') != undefined ) {
    
      await DecreaseQuantityButton(e.target.getAttribute('a-key'))

  

      await UpdateCart()

      }
  }

  


  async function UpdateCart(){
  
        fetch(`${process.env.REACT_APP_API_URL}/users/increasequantity`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                cart: updatedCart
            })    
        })
        .then(res => res.json())
        .then(data => {

            if (data) {

                    fetch(`${process.env.REACT_APP_API_URL}/users/increasequantity`, {
                        method: "PUT",
                        headers: {
                                'Content-Type': 'application/json',
                                Authorization: `Bearer ${localStorage.getItem('token')}`
                        },
                            body: JSON.stringify({
                            cart: updatedCart
                            })  
                        })
                        .then(res => res.json())
                        .then(data => {

                            setUser({
                             id: user.id,
                             isAdmin: user.isAdmin,
                             cart: updatedCart
                            });

                            console.log(data);
                            console.log(user);
                          
                           
                        })

            }else{
                Swal.fire({
                    text: "Opps Something went wrong :(",
                    icon: "error",
                    title: "Please try again!"
                })
            }
        })
  }
 

  return (
    <Modal
    {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >

    { ( user.cart.length != null && user.cart.length != undefined && user.cart.length != 0) ?
    <>  
      <Modal.Body>
        {user.cart.map(item => (
        <div id="cartItems" className="row text-center" >
            
                <div className="p-4 col-4"> {item.product_name} </div>
                <div className="p-4 col-4"> 
                      <div className='row'>
                      <Button className="col-4" variant="success" a-key={item.product_id}onClick={AddQuantity}> 
                          <AiFillPlusSquare /> 
                      </Button>
                      
                      <InputGroup.Text 
                      className='input-quanity col-4'>
                      <Form.Control 
                      type="number" 
                      placeholder="Enter Price"
                      value = {item.quantity} 
                      onChange = {e => setProductId(item.product_id)}
                      className ="container-fluid" 
                      />
                      </InputGroup.Text>
                      <Button className="col-4" variant="danger" a-key={item.product_id}onClick={DecreaseQuantity}> 
                          <AiFillMinusSquare /> 
                      </Button>
                </div>
                </div>
                <div className="p-4 col-4">PHP {item.price} </div>
            
        </div>
       
        ))}

        </Modal.Body>

         <h5 className ="text-center text-divider" ><span>Total Amount</span></h5> 
        <div className="text-center col d-flex flex-row-reverse">
         <h2 className="pe-5 pb-4">PHP {sum}</h2>
        </div>
        <Modal.Footer>
        <Button variant="outline-dark" floated='left' onClick={emptyCart}>Empty Cart</Button>
        <Button variant ="dark"onClick={props.onHide} as={Link} to={`/checkout`}>Check Out</Button>
        </Modal.Footer>
    </>

    :
    <>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Oppps!
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>Your Cart is currently Empty!</h4>
        <p>
         It would make you very happy if you added an item to the cart
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="dark"onClick={props.onHide}>Close</Button>
      </Modal.Footer>
      </>
    }
    </Modal>
  )
}





  return (
    <>
   
     <p className ="text-center pt-1" id="NavBar" >Free delivery from Php6,000 - Returns within 30 days</p>
    
    <div className="container-fluid mb-5 row ps-5 pt-4" bg="light" id="NavBar">
   
    <Navbar  expand="lg" className="col" sticky="top">
        <Navbar.Brand as={Link} to="/"><img src="https://iili.io/Hvoqre2.png" width="80" height="80" className="align-top rounded-circle " alt=""></img></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
          <Nav.Link className="topnav" as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link className="topnav" as={NavLink} to="/products">Shop Now</Nav.Link>
          {(user.isAdmin === true) ?
            <NavDropdown title="Admin">
              <NavDropdown.Item as={NavLink} to="/addproduct">Add Product</NavDropdown.Item>
              <NavDropdown.Divider/>
              <NavDropdown.Item href="/">User Maintenance</NavDropdown.Item>
            </NavDropdown>
            :
            <>
            </>
          }
          { (user.id !== null) ?
          <Nav.Link className="topnav" as={NavLink} to="/logout">Logout</Nav.Link>
          :
          <>
          <Nav.Link className="topnav" as={NavLink} to="/login">Login</Nav.Link>
         {/*<Nav.Link as={NavLink} to="/register">Register</Nav.Link> */}
          </>
          }         
          </Nav>
        </Navbar.Collapse>
          <div className="d-flex flex-row-reverse">
          
        { ( user.cart.length != null && user.cart.length != undefined && user.cart.length != 0) ?
          
           <div  className = "col-2 ">
           <div className="d-flex flex-column justify-content-center align-items-center pl-3 pr-3">
                <Button variant="dark" className="pl-3" onClick={() => setModalShow(true)} ><BsFillCartCheckFill/> {user.cart.length} </Button>
                <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                />
            </div>    
            </div>

        :
          <div className = "col-2">
            <div className="d-flex flex-column justify-content-center align-items-center pl-3 pr-3">
              <Button variant="outline-dark" className="pl-3" onClick={() => setModalShow(true)}><BsFillCartCheckFill/> 0</Button>
               <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                />
              </div>  
          </div>
        }

        <div className="col-4 ">
        <div className="pr-3">
        <InputGroup id="SearchBar">
              {/*<InputGroup.Text></InputGroup.Text>*/}
              
              <input placeholder="Search..." ></input>
              
        </InputGroup>
         </div>
        </div>
         </div>


    </Navbar>

    </div>
    </>
  );

}
// export default AppNavbar;