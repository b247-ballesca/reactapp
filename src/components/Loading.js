import Spinner from 'react-bootstrap/Spinner';

export default function Loading() {
  return (
    <>
      <div id="Loading" className="p-auto m-auto">
      <Spinner animation="grow" variant="dark"  />
      </div>
    </>
  );
}
