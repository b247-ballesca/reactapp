

import { Card, Button, Row, Col } from 'react-bootstrap';
import CardGroup from 'react-bootstrap/CardGroup';
import { useState, useEffect, useContext } from 'react'
import PropTypes from 'prop-types';
import UserContext from '../UserContext';
import AppNavbar from '../components/AppNavbar';
import Loading from '../components/Loading';
import {useNavigate, Navigate} from 'react-router-dom' 
import Products from '../pages/Products';
import { BsFillCartCheckFill, BsFillCartPlusFill } from 'react-icons/bs'


import { Link } from 'react-router-dom'

import Swal from 'sweetalert2';



export default function ProductCard({product}){

    const {product_name, description, price, _id, isActive, color, size, image_link} = product;
    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate();
    const [updatedIsActive, setUpdatedIsActive] = useState(!isActive);
    let updatedCart = []
    const [addCartItems, setAddCartItems] = useState("");

    //const [setIsActive] = useState("");
    const [productId, setProductId] = useState("");
    
    let updatedItemList

    function updateIsActive(e){
          if((updatedIsActive.toString()) === "true"){
            setUpdatedIsActive(false)
            }else{
            setUpdatedIsActive(true)
            } 

        alert(updatedIsActive)
         alert(_id)
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: updatedIsActive
            })    
        })
        .then(res => res.json())
        .then(data => {
            if (data) {

                Swal.fire({
                    text: `Product ${product_name} succesfully updated!`,
                    icon: "success",
                    title: "Great Job!"
                })
            }else{
                Swal.fire({
                    text: "Opps Something went wrong",
                    icon: "error",
                    title: "Please try again!"
                })
            }

            navigate("/products") 
        })
    
   }


  
     const pushIsActive = () => {
        //e.preventDefault()
        
        setUpdatedIsActive(!updatedIsActive)
      
        updateIsActive()
     }
        
    let checkItemExist = true
    
    async function CheckArray(){


         const pushArray = async () => {
                 updatedItemList = await Promise.all(user.cart.map( async (item) => {
                 if (item.product_id === _id) {
                    
                    checkItemExist =   false

                    return { ...item, quantity: item.quantity + 1, price: item.price * (item.quantity + 1)};
                 } 

                 return item;
                }));



            
             }
                await pushArray()
                updatedCart = updatedItemList
                setAddCartItems(updatedItemList)
    }

    async function AddNewItemToCart (){
        updatedCart = [...updatedCart,{product_id: _id,
                        product_name: product_name,
                        price: price,
                        quantity: 1}]

    }


async function addToCart(){
updatedCart = []

      if( user.id != null){
        
    

        console.log(user)
        console.log(_id)

          if (user.cart.length > 0) {
          
               
                await CheckArray()
                
            

                if (checkItemExist === true ){

                    await AddNewItemToCart()

                    checkItemExist = true

                } 

                console.log(addCartItems)

                console.log(updatedCart)

                // alert("May Item!")

          } else if (user.cart.length === 0 || user.cart === 0 ) {

                await AddNewItemToCart()
                
          }
          
          //  const arrupdatedCart = updatedCart;

            
          // console.log(arrupdatedCart)
     
          //  let updatedCartArr = arrupdatedCart


        fetch(`${process.env.REACT_APP_API_URL}/users/addtocart`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                cart: updatedCart
            })    
        })
        .then(res => res.json())
        .then(data => {

            if (data) {

                 
            }else{
                Swal.fire({
                    text: "Opps Something went wrong :(",
                    icon: "error",
                    title: "Please try again!"
                })
            }

          
        })
         SavetoCart()
      } else {
        alert("Please login before adding any items to your cart!")
      }  

              
      }      
      

     async function SavetoCart(){
                    
               
                        const myData =  {
                                id: user.id,
                                isAdmin: user.isAdmin,
                                cart: updatedCart
                              };
                        const newPosts =  myData;
                       await setUser(newPosts);
              

                     

             console.log(user);               

      } 


              
                       
    return (
                
            <div className="col-3">
              <Card key={_id} className="productlist" id = "productCard">
                 <Card.Img variant="top" src={image_link} height = "250" id="ProductImages" />
                 <Card.Body >
                    <Card.Title className="fw-bold">{product_name}</Card.Title>
                    
                    <Card.Text>{description}</Card.Text>
                    <Card.Text className="text-danger fw-bold" float="Right">₱ {price}.00</Card.Text> 
                                  
                     
                    

                    {(updatedIsActive) ?
                    <div className="form-check form-switch">
                      <input className="form-check-input " 
                          type="checkbox" 
                          role="switch" 
                          id="flexSwitchCheckChecked" 
                          onChange={pushIsActive}
                          checked={false}
                          hidden={!user.isAdmin}
                           />
                      <label className="form-check-label" htmlFor="flexSwitchCheckChecked">Inactive</label>
                    </div>
                    :
                    <>
                    <div className="form-check form-switch ">
                      <input className="form-check-input bg-success" 
                          type="checkbox" 
                          role="switch" 
                          id="flexSwitchCheckChecked" 
                          onChange={pushIsActive}
                          checked={true} 
                          hidden={!user.isAdmin}
                          />
                      <label className="form-check-label" htmlFor="flexSwitchCheckChecked" hidden={!user.isAdmin}>Active</label>
                    </div>
                    </>
                    }
                    
                    <div className = "col-12">
                    <div className="d-flex flex-column justify-content-center align-items-center pl-3 pr-3">
                    {(user.isAdmin === true) ?
                     <Button className= "col mb-1" variant="dark" as={Link} to={`/editproduct/${_id}`}>Edit Product</Button>
                    :
                    <>
                    <Button className= "col mb-1" variant="dark" as={Link} to={`/viewproduct/${_id}`}>View Product</Button>

                    <Button  variant="outline-dark col" onClick={addToCart}>Add to Cart <BsFillCartPlusFill/> </Button>
                    </>
                    }
                    </div>
                    </div>
                </Card.Body>
             </Card>
          </div>
           
    )


}   



 


    ProductCard.propTypes = {
        product: PropTypes.shape({
            // Define the properties and their expected types
            product_name: PropTypes.string.isRequired,
            description: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
           
        })
    }
 
//product Card