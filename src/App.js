import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import AddProduct from './pages/AddProduct';
import EditProduct from './pages/EditProduct';
import Home from './pages/Home'
import Products from './pages/Products'
import CheckOut from './pages/CheckOut'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import ProductCard from './components/ProductCard';
import './App.css';

import { useState, useEffect } from 'react';

import { UserProvider } from './UserContext';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';


function App() {

  // State hook for the user state that's defined here is for a global scope
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  //const [user, setUser] = useState({ email : localStorage.getItem("email") })
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    cart: 0
  });
 //  

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => { 
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          cart: data.cart
        })

         
         console.log(user)
         console.log(data)
      } else {
        setUser({
          id: null,
          isAdmin: null,
          cart: 0
        })

   
      }
      
    })
  }, []);

  // Common pattern in react.js for a component to return multiple elements.
  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products/>} />
            <Route path="/addproduct" element={<AddProduct/>} /> 
            <Route path="/checkout" element={<CheckOut/>} />
            <Route path="/editproduct/:productId" element={<EditProduct/>} />
            <Route path="/viewproduct/:productId" element={<ProductView/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/logout" element={<Logout/>} />
            {/*
              "*" - is a wildcard character that will match any path that has not already been matched by previous routes.
            */}
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
    </Router>
    </UserProvider>
    </>
  );
}

export default App;